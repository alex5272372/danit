let currentFolder = 'step02';

const gulp = require('gulp');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

// очистка папки dist
function cleanFolder() {
    return gulp
        .src(`./${currentFolder}/dist/*`, {read: false})
        .pipe(clean());
}

// компиляция scss файлов в css
// добавление вендорных префиксов к CSS свойствам для поддержки последних нескольких версий каждого из браузеров
// конкатенация css файлов в один
// минификация итогового css файла
// копирование минифицированного css файла в папку dist
function makeCSS() {
    return (
        gulp
            .src(`./${currentFolder}/src/scss/*.scss`)
            .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
            .pipe(autoprefixer())
            .pipe(concat('styles.css'))
            .pipe(cleanCSS({compatibility: 'ie8'}))
            .pipe(gulp.dest(`./${currentFolder}/dist/css`))
    );
}

// конкатенация js файлов в один
// минификация итогового js файла
// копирование минифицированного js файла в папку dist
function makeJS() {
    return (
        gulp
            .src(`./${currentFolder}/src/js/*.js`)
            .pipe(concat('all.js'))
            .pipe(uglify())
            .pipe(gulp.dest(`./${currentFolder}/dist/js`))
    );
}

// оптимизация картинок и копирование их в папку dist
function makeImages() {
    gulp
        .src(`./${currentFolder}/src/img/*`)
        .pipe(imagemin())
        .pipe(gulp.dest(`./${currentFolder}/dist/img`))
}

function build() {
    cleanFolder();
    makeCSS();
    makeJS();
    makeImages();
}

function autoReload(done) {
    browserSync.reload();
    done();
}

function dev() {
    browserSync.init({
        server: {
            baseDir: `./${currentFolder}`
        }
    });
    gulp.watch(`./${currentFolder}/src/scss/*.scss`, makeCSS);
    gulp.watch(`./${currentFolder}/src/js/*.js`, makeJS);
    gulp.watch(`./${currentFolder}/dist/**/*`, autoReload);
}

gulp.task('cleanFolder', cleanFolder);
gulp.task('makeCSS', makeCSS);
gulp.task('makeJS', makeJS);
gulp.task('makeImages', makeImages);
gulp.task('build', build);
gulp.task('autoReload', autoReload);
gulp.task('dev', dev);