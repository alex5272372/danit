UPD: Все замечания устранены.

# Учебные проекты [DAN.IT](https://dan-it.com.ua)
[Работа в команде на степ-проекте](https://dan-it.gitlab.io/fe-book/teamwork/step.html)

[Работа в команде на финальном проекте](https://dan-it.gitlab.io/fe-book/teamwork/final.html)

## [Степ проект 2](https://gitlab.com/dan-it/groups/fe5/tree/master/step-project-forkio)

### Задание №1
Исполнитель: Andrii Golik

Секции: nav, header, people

### Задание №2
Исполнитель: Alexey Nikolaenko

Секции:  present ,features, price

[Замечания по домашней работе Ira Stoetskaya](https://docs.google.com/document/d/1fO-C6vsUCKZ6ntF4w4lgE98Xn1k4hd7JxUHmZgxUEz0/edit)

[Замечания по степ проекту Ira Stoetskaya](https://docs.google.com/document/d/1LkD63jmSfbMHdxX-FtH3d9Wj6cEUDJLrQUanM-YBe0M/edit)

